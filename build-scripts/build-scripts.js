//Creates the files "dist/aak-cont-script-notubo.user.js" and "dist/aak-cont-script-ubo.user.js"
//Expects "build-filters.js", "build-ubp-rules-conversion.js", and "build-uboruntime-with-rules.js" just ran

"use strict";

const linebreak = process.argv[2] === "--windows" ? "\r\n" : "\n";

const fs = require("fs");

const metaScriptPath = "../src/aak-cont-script-meta.js";
const coreScriptPath = "../src/aak-cont-script-core.js";
const ubpRulesPath = "tmp/ubp-rules.js";
const aakRulesPath = "../src/aak-cont-script-rules.js";
const uboRuntimePath = "tmp/uboruntime-with-rules.js";
const outputUboPath = "../dist/aak-cont-script-ubo.user.js";
const outputNotUboPath = "../dist/aak-cont-script-notubo.user.js";
const versionFile = "version.json";

const metaScriptContentLines = fs.readFileSync(metaScriptPath).toString().split("\n");
const coreScriptContent = fs.readFileSync(coreScriptPath).toString();
const ubpRulesContent = fs.readFileSync(ubpRulesPath).toString();
const aakRulesContent = fs.readFileSync(aakRulesPath).toString();
const uboRuntimeContent = fs.readFileSync(uboRuntimePath).toString();
const version = JSON.parse(fs.readFileSync(versionFile));

// Replace meta @pragma tags
let processedMeta_uBO = [];
let processedMeta_NotuBO = [];
{
    let i;
    for (i = 0; i < metaScriptContentLines.length; i++) {
        let entry = metaScriptContentLines[i].trim();

        switch (entry) {
            case "@pragma-insert-title":
                processedMeta_uBO.push("// @name AAK-Cont Userscript For uBlock Origin");
                processedMeta_NotuBO.push("// @name AAK-Cont Userscript For AdBlock, Adblock Plus, etc");
                break;
            case "@pragma-insert-version":
                processedMeta_uBO.push(`// @version ${version.version}`);
                processedMeta_NotuBO.push(`// @version ${version.version}`);
                break;
            case "@pragma-insert-urls":
                processedMeta_uBO.push(
                    `// @updateURL https://gitlab.com/xuhaiyang1234/AAK-Cont/raw/master/dist/aak-cont-script-ubo.user.js` + linebreak +
                    `// @downloadURL https://gitlab.com/xuhaiyang1234/AAK-Cont/raw/master/dist/aak-cont-script-ubo.user.js`
                );
                processedMeta_NotuBO.push(
                    `// @updateURL https://gitlab.com/xuhaiyang1234/AAK-Cont/raw/master/dist/aak-cont-script-notubo.user.js` + linebreak +
                    `// @downloadURL https://gitlab.com/xuhaiyang1234/AAK-Cont/raw/master/dist/aak-cont-script-notubo.user.js`
                );
                break;
            default:
                processedMeta_uBO.push(entry);
                processedMeta_NotuBO.push(entry);
        }
    }
}

fs.writeFileSync(outputUboPath, processedMeta_uBO.join(linebreak) + linebreak + coreScriptContent + linebreak + ubpRulesContent + linebreak + aakRulesContent);
fs.writeFileSync(outputNotUboPath, processedMeta_NotuBO.join(linebreak) + linebreak + coreScriptContent + linebreak + ubpRulesContent + linebreak + aakRulesContent + linebreak + uboRuntimeContent);
