//Build filter lists

"use strict";

const linebreak = process.argv[2] === "--windows" ? "\r\n" : "\n";

const {
    readFileSync: read,
    writeFileSync: write,
    existsSync: exist,
    createReadStream: readStream,
    createWriteStream: writeStream,
} = require("fs");

//First see if we can find uBlock Protector repository
const uBlockProtectorFilter = "../../uBlockProtector/uBlockProtectorList.txt";
//This file will be used if we cannot find the other repository
const uBlockProtectorFilterFallback = "cache/uBlockProtectorList.txt";
//The meta file
const metaFile = "../src/filer-meta.txt";
//uBO extra filters
const uBOExtraFilters = "../src/filter-extra-ubo.txt";
//Not uBO extra filters
const NotuBOExtraFilters = "../src/filter-extra-notubo.txt";
//Version configuration
const versionFile = "version.json";

const uBOOutput = "../dist/aak-cont-list-ubo.txt";
const NotuBOOutput = "../dist/aak-cont-list-notubo.txt";

//Read files
let filter;
try {
    filter = read(uBlockProtectorFilter);
} catch (e) {
    filter = read(uBlockProtectorFilterFallback);
}
filter = filter.toString().split("\n");

const meta = read(metaFile).toString().split("\n");
const uBOExtra = read(uBOExtraFilters).toString().split("\n");
const NotuBOExtra = read(NotuBOExtraFilters).toString().split("\n");
const version = JSON.parse(read(versionFile));

version.version = parseFloat(version.version) + 0.001;
version.version = String(Math.round(version.version * 1000) / 1000);

//Parse filter
let uBO = [];
let NotuBO = [];
let line;
{
    let isForuBO = true, i;
    for (i = 0; i < filter.length; i++) {
        let entry = filter[i].trim();

        (entry === "! =====Patched Anti-Adblock Killer List (originally by Reek)=====") && (isForuBO = false);

        if (entry.charAt(0) === "!" || entry === "")
            continue;

        if (entry === "jspenguin2017.github.io##script:inject(doubleclick.net/instream/ad_status.js)")
            continue;

        isForuBO ? uBO.push(entry) : NotuBO.push(entry);
    }
}

//Process other parts
let processedMeta_uBO = [];
let processedMeta_NotuBO = [];
{
    let i;
    for (i = 0; i < meta.length; i++) {
        let entry = meta[i].trim();

        switch (entry) {
            case "@pragma-insert-title":
                processedMeta_uBO.push("! Title: AAK-Cont Filter For uBlock Origin");
                processedMeta_NotuBO.push("! Title: AAK-Cont Filter For AdBlock, Adblock Plus, etc");
                break;
            case "@pragma-insert-version":
                processedMeta_uBO.push(`! Version: ${version.version}`);
                processedMeta_NotuBO.push(`! Version: ${version.version}`);
                break;
            default:
                processedMeta_uBO.push(entry);
                processedMeta_NotuBO.push(entry);
        }
    }
}

//Process extra filters
{
    let i;
    for (i = 0; i < uBOExtra.length; i++) {
        uBO.push(uBOExtra[i].trim());
    }
    for (i = 0; i < NotuBOExtra.length; i++) {
        NotuBO.push(NotuBOExtra[i].trim());
    }
}

//Write output
write(uBOOutput, processedMeta_uBO.concat(uBO).concat(NotuBO).join(linebreak));
write(NotuBOOutput, processedMeta_NotuBO.concat(NotuBO).join(linebreak));
//Save version
write(versionFile, JSON.stringify(version));

//Update cache
if (exist(uBlockProtectorFilter)) {
    readStream(uBlockProtectorFilter).pipe(writeStream(uBlockProtectorFilterFallback));
}
